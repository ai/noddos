// $Id$

#include <numeric>
#include <iostream>
#include <sstream>
#include <string>
#include "status_httpd.h"

using namespace std;

namespace dos {

static void normalize(vector<double>& values)
{
  ostringstream ds;
  double min = min_element(values.begin(), values.end());
  double max = max_element(values.begin(), values.end());
  vector<double>::iterator i;
  for (i = values.begin(); i != values.end(); ++i)
    *i = (*i - min) / (max - min);
}

static string to_dataset(vector<double>& values)
{
  static const char vchar[] = 
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  normalize(values);
  ostringstream ds;
  vector<double>::const_iterator i;
  for (i = values.begin(); i != values.end(); ++i)
    ds << vchar[static_cast<int>(100 * (*i))];
  return ds.str();
}

static string qps_graph(const vector<QpsTuple>& qps_values, 
                        int width=500, int height=200) 
{
  ostringstream url;
  url << "http://chart.apis.google.com/chart"
      << "?chs=" << width << "x" << height
      << "&cht=lc";

  vector<double> stdup, avg, stddown;
  vector<QpsTuple>::const_iterator i;
  for (i = qps_values.begin(); i != qps_values.end(); ++i) {
    stdup.push_back((*i).value + (*i).std_dev);
    avg.push_back((*i).value);
    stddown.push_back((*i).value - (*i).std_dev);
  }

  url << "&chd=s:"
      << to_dataset(stdup) << ","
      << to_dataset(avg) << ","
      << to_dataset(stddown);
}

void StatusHttpd::handle_status(evhttp_request *req)
{
  ostringstream resp;

  resp << "<?xml version=\"1.0\"?>\n"
       << "<html lang=\"en\">\n"
       << "<head><title>NoDDoS master</title>\n"
       << "<link rel=\"stylesheet\" type=\"text/css\" href=\"/style.css\"/>\n"
       << "</head><body>\n";

  resp << "<h1>NoDDoS Master</h1>";

  vector<QpsTuple> qps_values;
  logic->get_store()->read_stats(qps_values, 24);
  string graph_url = qps_graph(qps_values);
  resp << "<table><tr><td valign=\"top\">"
       << "<p>Global QPS: <b>" << logic->get_qps() << "</b></p>"
       << "<p>Global status: <b>" << logic->get_status().str() 
       << "</b></p>\n"
       << "</td><td>"
       << "<img src=\"" << graph_url << "\" border=\"0\"/>"
       << "</td></tr></table>\n";

  resp << "<h3>Nodes</h3><ul>";
  vector<string> nodes = logic->nodes();
  vector<string>::const_iterator i;
  for (i = nodes.begin(); i != nodes.end(); i++)
    resp << "<li><a href=\"http://" << (*i)
         << "/\">" << (*i) << "</a></li>";
  resp << "</ul>\n";

  resp << "<h3>Blacklisted hosts</h3><table class=\"d\">"
       << "<thead><tr><th>IP</th></tr></thead><tbody>";
  vector<string> blacklist = logic->get_blacklist();
  for (i = blacklist.begin(); i != blacklist.end(); i++)
    resp << "<tr><td>" << (*i) << "</td></tr>";
  resp << "</tbody></table>\n";

  resp << "</body></html>\n";

  send_reply(req, 200, "OK", resp.str().c_str(), resp.str().size());
}

} // namespace
