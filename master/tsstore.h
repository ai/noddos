// $Id$

#ifndef __tsstore_H
#define __tsstore_H 1

#include <iostream>
#include <string>
#include <vector>
#include <sys/types.h>
#include <stdlib.h>
#include <sqlite3.h>

namespace dos {

struct TimeTuple
{
  TimeTuple() 
    : hour(0), weekday(0)
  {}

  TimeTuple(int hour_, int weekday_)
    : hour(hour_), weekday(weekday_)
  {}

  TimeTuple(time_t ts);

  int hour;
  int weekday;
};

struct QpsTuple
{
  QpsTuple(double value_=0, double std_dev_=0)
    : value(value_), std_dev(std_dev_)
  {}

  TimeTuple stamp;
  double value;
  double std_dev;
};

class TSStore
{
public:
  TSStore (const char *dbn_ = "default.db") 
    : dbname(dbn_) {
    /* open connection and init database */
    db_init();
  }

  bool store_hourly_stats (time_t stamp, const TimeTuple& key, 
			   double qps, double std_dev);

  bool query(const TimeTuple& key, double *qps, double *std_dev);
  
  double wd_mean (std::vector<double>& wm);

  double wd_std_dev (std::vector<double>& wstd);

  bool read_stats(const TimeTuple& target_key,
                  std::vector<QpsTuple>& out,
                  int hours = 24);

private:
  void db_init (void);
  
  int prepare (const std::string& sqlqry){
    int r;
    r = sqlite3_prepare_v2(db, sqlqry.c_str(), -1, &stmt, NULL);
    if (r != SQLITE_OK) {
      std::cerr << "error preparing query: " << sqlite3_errmsg(db)
                << std::endl;
      exit(1);
    }
    return r;
  }

  int step (void) {
    int r;
    r = sqlite3_step (stmt);
    return r;
  }

  int finalize (void) {
    int r;
    r = sqlite3_finalize (stmt);
    if (r != SQLITE_OK) {
      std::cerr << "error finalizing query: " << sqlite3_errmsg(db)
                << std::endl;
      exit(1);
    }
    return r;
  }

  std::string dbname;
  sqlite3 *db;
  sqlite3_stmt *stmt;
};

} // namespace

#endif
