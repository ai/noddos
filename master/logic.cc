// $Id$

#include <iostream>
#include <numeric>
#include <math.h>
#include "logic.h"
#include "common/log.h"
#include "common/util.h"
#include "net/dist.h"

namespace dos {

struct qps_adder {
  double operator()(double result, const std::pair<int, double>& arg) {
    return result + arg.second;
  }
};

double Receiver::qps() {
  return std::accumulate(node_qps.begin(), node_qps.end(), 0.0, qps_adder());
}

struct print_node_qps {
  void operator() (const std::pair<int, double>& x) {
    std::cout << "  " << x.first << ": " << x.second << " qps\n";
  }
};

void Receiver::dump_qps() {
  std::for_each(node_qps.begin(), node_qps.end(), print_node_qps());
}

struct send_status_to_node {
  send_status_to_node(const Status& status_)
    : status(status_) {}
  void operator() (const std::pair<int, NodeIp>& x) {
    dos::net::ProbeProxy probe(x.second.ip, x.second.port);
    probe.send_status(status.str());
  }
  Status status;
};

void Receiver::send_status(const Status& status) {
  std::for_each(node_ips.begin(), node_ips.end(), 
		send_status_to_node(status));
}

void Receiver::handle_blacklist(int node_id, const Ip& ip) {
  struct in_addr src;
  inet_aton(ip.ip().c_str(), &src);
  if (global_whitelist->match(src))
    return;
  global_blacklist.add(ip.ip());
  LOG4CXX_INFO(log::logger, "blacklisted " << ip.ip());
}

void Receiver::handle_qps(int node_id, const Qps& qps) {
  double value = qps.qps();
  // only react to known nodes
  if (node_ips.find(node_id) != node_ips.end())
    node_qps[node_id] = value;
}

void Receiver::add_node(const std::string& addr, int node_id) {
  NodeIp nip;
  if (!util::parse_addr(addr, nip.ip, &nip.port)) {
    LOG4CXX_ERROR(log::logger, "could not parse host:port from '" << addr << "'");
    return;
  }
  node_ips.insert(std::make_pair(node_id, nip));
  node_qps.insert(std::make_pair(node_id, 0.0));
}

void MasterLogic::tick() {
  cur_qps = receiver->qps();
  qcoll.push_value(cur_qps);
  TimeTuple cur_time(time(NULL));
  mstorage.query(cur_time, &expected_qps, &expected_std_dev);
  // If the expected qps value is 0, we probably don't have
  // enough data yet to make a decision. Fail open.
  if ((expected_qps > 0)
      && (expected_std_dev > 0)
      && (fabs(expected_qps - cur_qps) / expected_std_dev > 3)
      && !dont_panic) {
    set_status(Status::ATTACK);
  } else {
    set_status(Status::OK);
  }
  receiver->send_status(status);
}

void MasterLogic::set_status(Status new_status) {
  if (new_status != status) {
    status = new_status;
    LOG4CXX_INFO(log::logger, "global status changed to " 
                 << status.str());
  }
}

std::vector<std::string> MasterLogic::nodes() {
  std::map<int, NodeIp>::const_iterator i;
  std::vector<std::string> node_list;
  for (i = receiver->node_ips.begin(); i != receiver->node_ips.end(); i++) {
    std::ostringstream tmp;
    tmp << (*i).second.ip << ":" << (*i).second.port;
    node_list.push_back(tmp.str());
  }
  return node_list;
}

} // namespace dos
