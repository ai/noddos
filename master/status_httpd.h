// $Id$

#ifndef __master_status_httpd_H
#define __master_status_httpd_H 1

#include "logic.h"
#include "net/httpd.h"

namespace dos {

class StatusHttpd
  : public net::Httpd
{
public:
  StatusHttpd(net::Async *async_, int port_, MasterLogic *logic_)
    : net::Httpd(async_, port_), logic(logic_)
  {
    add_handler("/", httpd_callback(this, &StatusHttpd::handle_status));
    add_handler("/status", httpd_callback(this, &StatusHttpd::handle_status));
  }

  void handle_status(evhttp_request *req);

private:
  MasterLogic *logic;
};

} // namespace dos 

#endif
