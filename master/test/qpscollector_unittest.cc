// $Id: acl_unittest.cc 41 2009-09-28 18:41:59Z ale $

#include <math.h>
#include <gtest/gtest.h>
#include "common/log.h"
#include "master/qpscollector.h"
#include "common/test/test_timer.h"

using namespace std;
using namespace dos;

namespace {

static const char *TEST_DB = "__test.db";
static const int HOUR = 3600;
static TestTimer *mock_timer = new TestTimer();

class ControlledTimerEnvironment 
  : public testing::Environment
{
public:
  virtual void SetUp() {
    system_timer = mock_timer;
  }
};

class QpsCollectorTest
  : public testing::Test
{
public:
  void SetUp(void) {
    store = new TSStore(TEST_DB);
  }

  void TearDown(void) {
    delete store;
    unlink(TEST_DB);
  }

  TSStore *store;
};

TEST_F(QpsCollectorTest, SanityCheck) {
  mock_timer->set(0);
  QpsCollector coll(store);
  coll.push_value(1);
}

TEST_F(QpsCollectorTest, PushesHourly) {
  mock_timer->set(0);
  QpsCollector coll(store);
  coll.push_value(1);
  mock_timer->set(1*HOUR);
  coll.push_value(1);
  
  double mean = 0, std_dev = 0;
  store->query(TimeTuple(0), &mean, &std_dev);
  EXPECT_EQ(1, mean);
  EXPECT_EQ(0, std_dev);
}

TEST_F(QpsCollectorTest, ComputesHourlyMean) {
  mock_timer->set(0);
  QpsCollector coll(store);
  double values[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  int n_values = 10;
  for (int i=0; i<n_values; i++)
    coll.push_value(values[i]);
  mock_timer->set(3600);
  coll.push_value(1);

  double mean = 0, std_dev = 0;
  store->query(TimeTuple(0), &mean, &std_dev);

  // compute the expected values
  double exp_mean = 0, exp_std_dev = 0;
  for (int i=0; i<n_values; i++)
    exp_mean += values[i];
  exp_mean /= n_values;
  for (int i=0; i<n_values; i++)
    exp_std_dev += pow(values[i] - exp_mean, 2);
  exp_std_dev = sqrt(exp_std_dev / n_values);

  EXPECT_NEAR(exp_std_dev, std_dev, 1.0e-4);
  EXPECT_NEAR(exp_mean, mean, 1.0e-4);
}

} // namespace

int main(int argc, char **argv) {
  dos::log::init();
  testing::InitGoogleTest(&argc, argv);
  testing::AddGlobalTestEnvironment(new ControlledTimerEnvironment);
  return RUN_ALL_TESTS();
}
