// $Id$

#ifndef __qpscollector_H
#define __qpscollector_H 1

#include "tsstore.h"
#include "common/timer.h"

namespace dos {

class QpsCollector
{
public:
  QpsCollector(TSStore *storage_);
    
  /* hourly arithmetic mean */
  double avg_hourly_mean(const std::vector<double>& v);
  
  /* corrected standard deviation */
  double avg_hourly_std_dev(const std::vector<double>& v, double mean);

  /* push a value into the hourly slot */
  void push_value (double val);

private:
  TSStore *storage;
  TimeTuple cur_time;
  std::vector< std::vector<double> > avgs;
};

} // namespace dos

#endif
