// $Id$

#include <iostream>
#include <numeric>
#include <math.h>
#include "common/log.h"
#include "qpscollector.h"

namespace dos{

QpsCollector::QpsCollector(TSStore *storage_)
  : storage(storage_), cur_time(system_timer->now())
{
  for (int i = 0; i < 24; i++)
    avgs.push_back(std::vector<double> ());
}

double QpsCollector::avg_hourly_mean(const std::vector<double>& v) {
  double sum = std::accumulate(v.begin(), v.end(), 0.0);
  if (v.size() > 1)
    return (sum / v.size());
  else
    return sum;
}

double QpsCollector::avg_hourly_std_dev(const std::vector<double>& v, double mean) {
  int n = v.size();
  double std_dev = 0;

  for (std::vector<double>::const_iterator i = v.begin(); i != v.end(); i++)
    std_dev += pow(*i - mean, 2);
  if (n > 0)
    std_dev = sqrt(std_dev / n);
  return std_dev;
}
  
void QpsCollector::push_value(double val) {
  // Report the time as 1 hour ago
  time_t now = system_timer->now();
  TimeTuple new_time(now);

  if (new_time.hour != cur_time.hour) {
    std::vector<double>& hourdata = avgs[cur_time.hour];
    double mean = avg_hourly_mean(hourdata);
    double std_dev = avg_hourly_std_dev(hourdata, mean);

    LOG4CXX_DEBUG(log::logger, "slot "<< cur_time.hour
		  << ", mean=" << mean
		  << " std dev=" << std_dev
		  << " (on "<< hourdata.size() << " elements)");
    hourdata.clear();

    // Store the hourly average with the weekday/hour key
    storage->store_hourly_stats(now, cur_time, mean, std_dev);

    cur_time = new_time;
  }

  // Compute the hourly average
  avgs[cur_time.hour].push_back(val);
}

} // namespace dos
