// $Id$

#include "logic.h"
#include "status_httpd.h"
#include "common/daemon.h"
#include "common/log.h"
#include "common/util.h"
#include <iostream>
#include <fstream>
#include <getopt.h>
#include <stdlib.h>

using namespace dos;
using namespace dos::net;

struct option longopts[] = {
  {"help", no_argument, NULL, 'h'},
  {"debug", no_argument, NULL, 'd'},
  {"database", required_argument, NULL, 'D'},
  {"whitelist", required_argument, NULL, 'w'},
  {"nodes", required_argument, NULL, 'N'},
  {"pidfile", required_argument, NULL, 'P'},
  {"port", required_argument, NULL, 'p'},
  {"foreground", no_argument, NULL, 'F'},
  {"pretend", no_argument, NULL, '1'},
  {NULL, 0, NULL, 0}
};

static void usage()
{
  std::cout << "Usage: probe [<OPTIONS>]\n"
            << "Known options:\n"
            << "  -d, --debug\n"
            << "          Log debug messages.\n"
            << "  -D, --database FILE\n"
            << "          Path to the database file (default /var/lib/noddos/qps.db)\n"
            << "  -w, --whitelist FILE\n"
            << "          Reads the whitelist from FILE (in CIDR format)\n"
            << "  -N, --nodes FILE\n"
            << "          Read the list of nodes from FILE (format is 'address:port node_id')\n"
	    << "  --pidfile FILE\n"
	    << "          Write the process pid to this file.\n"
	    << "  --port PORT\n"
	    << "          Bind to this UDP port (default 6454)\n"
	    << "  -F, --foreground\n"
	    << "          Do not fork into the background.\n"
            << "  --pretend\n"
            << "          Turn off actual attack detection. Useful for testing\n"
            << "          and to accumulate enough initial baseline data.\n" 
            << "\n";
  exit(0);
}

struct acl_matcher_adder {
  AclMatcher *whitelist;
  acl_matcher_adder(AclMatcher *w) : whitelist(w) {}
  bool operator()(const std::vector<std::string>& line) {
    if (line.size() != 1)
      return false;
    whitelist->add(line[0].c_str());
    return true;
  }
};

static AclMatcher *create_whitelist(const char *whitelist_file)
{
  AclMatcher *whitelist = new AclMatcher;
  if (whitelist_file)
    util::read_file(whitelist_file, acl_matcher_adder(whitelist));
  return whitelist;
}

struct receiver_adder {
  Receiver *receiver;
  receiver_adder(Receiver *r) : receiver(r) {}
  bool operator()(const std::vector<std::string>& line) {
    if (line.size() != 2)
      return false;
    receiver->add_node(line[0], util::to_int(line[1]));
    return true;
  }
};

static void read_nodelist(const char *nodelist_file, Receiver *receiver)
{
  util::read_file(nodelist_file, receiver_adder(receiver));
}

static void run(const char *nodelist_file, 
                const char *whitelist_file,
                const char *database_file,
                int port, bool no_attack_detection)
{
  Async async;
  AclMatcher *whitelist = create_whitelist(whitelist_file);
  Receiver *receiver = new Receiver(whitelist, port);
  receiver->add(&async);
  MasterLogic *logic = new MasterLogic(receiver, 120, database_file, no_attack_detection);
  read_nodelist(nodelist_file, receiver);
  logic->add(&async);
  StatusHttpd httpd(&async, port, logic);
  LOG4CXX_INFO(log::logger, "starting master");
  async.run();
}

int main(int argc, char **argv)
{
  char *nodelist_file = NULL;
  char *whitelist_file = NULL;
  char *pid_file = NULL;
  char *database_file = "/var/lib/noddos/qps.db";
  bool debug = false;
  bool background = true;
  bool pretend = false;
  int port = 6454;
  int c;

  while ((c = getopt_long(argc, argv, "dD:Fw:N:h", longopts, NULL)) > 0) {
    switch (c) {
    case 'd':
      debug = true;
      break;
    case 'D':
      database_file = optarg;
      break;
    case 'w':
      whitelist_file = optarg;
      break;
    case 'N':
      nodelist_file = optarg;
      break;
    case 'F':
      background = false;
      break;
    case 'P':
      pid_file = optarg;
      break;
    case 'p':
      port = atoi(optarg);
      break;
    case '1':
      pretend = true;
      break;
    default:
      usage();
    }
  }
  if (!nodelist_file) {
    std::cerr << "Error: must specify a node list with --nodes\n";
    usage();
  }

  if (background)
    util::daemonize(0, pid_file);

  log::init(background);
  if (debug)
    log::logger->setLevel(log4cxx::Level::toLevel(log4cxx::Level::DEBUG_INT));

  run(nodelist_file, whitelist_file, database_file, port, pretend);

  // unreached
  return 0;
}

