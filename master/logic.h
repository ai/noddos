// $Id$ -*-c++-*-

#ifndef __master_logic_H
#define __master_logic_H 1

#include "common/acl.h"
#include "common/blacklist.h"
#include "common/ntm.h"
#include "common/status.h"
#include "common/timer.h"
#include "net/dist.h"
#include "qpscollector.h"
#include <string>

namespace dos {

struct NodeIp
{
  char ip[16];
  int port;
};

class Receiver
  : public dos::net::MasterImpl
{
public:
  Receiver(AclMatcher *global_whitelist_, int port_)
    : dos::net::MasterImpl(port_),
      global_whitelist(global_whitelist_)
  {}

  double qps();
  void dump_qps();

  virtual void handle_blacklist(int node_id, const Ip& ip);

  virtual void handle_qps(int node_id, const Qps& qps);

  void send_status(const Status& status);

  void add_node(const std::string& addr, int node_id);

  AclMatcher *global_whitelist;
  Blacklist global_blacklist;
  std::map<int, double> node_qps;
  std::map<int, NodeIp> node_ips;
};

class MasterLogic
  : public dos::net::Timer
{
public:
  MasterLogic(Receiver *receiver_, int period_, 
              const char *database_path_, 
              bool dont_panic_=false)
    : dos::net::Timer(period_), receiver(receiver_),
      mstorage(database_path_), qcoll(&mstorage),
      cur_qps(0), expected_qps(0), expected_std_dev(0),
      status(Status::OK), dont_panic(dont_panic_)
  {}

  virtual void tick();

  void set_status(Status new_status);

  Status get_status() { return status; }

  double get_qps() { return cur_qps; }

  double get_expected_qps() { return expected_qps; }

  double get_expected_std_dev() { return expected_std_dev; }

  std::vector<std::string> nodes();

  std::vector<std::string> get_blacklist() {
    return receiver->global_blacklist.list();
  }

protected:
  Receiver *receiver;
  TSStore mstorage;
  QpsCollector qcoll;
  Status status;
  double cur_qps;
  double expected_qps;
  double expected_std_dev;
  bool dont_panic;
};

} // namespace dos

#endif
