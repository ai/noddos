// $Id$

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <sqlite3.h>
#include <math.h>
#include "common/log.h"
#include "common/timer.h"
#include "tsstore.h"

namespace dos {

static const int ONE_MONTH_IN_SECONDS = 86400 * 30;

TimeTuple::TimeTuple(time_t ts) {
  struct tm tinfo;
  (void)gmtime_r(&ts, &tinfo);
  weekday = tinfo.tm_wday;
  hour = tinfo.tm_hour;
}

void TSStore::db_init() {
  int ret;
  std::string q("create table if not exists hourly_data(\
                 stamp integer,\
                 mean double,\
                 std_dev double,\
                 h smallint,\
                 wd smallint)");

  ret = sqlite3_open(dbname.c_str(), &db);
  if (ret) {
    LOG4CXX_ERROR(log::logger, "cannot open database: " << sqlite3_errmsg(db));
    sqlite3_close(db);
    exit(1);
  }

  ret = this->prepare(q);

  ret = this->step();
  if (ret != SQLITE_DONE) {
    LOG4CXX_ERROR(log::logger, "could not execute the query: " << sqlite3_errmsg(db));
    exit(1);
  }

  ret = this->finalize();

}

bool TSStore::store_hourly_stats(time_t stamp, 
				 const TimeTuple& key, 
				 double mean, double std_dev) {
  std::ostringstream qtmp;
  int ret;

  qtmp << "insert into hourly_data (stamp, wd, h, mean, std_dev) "
       << "values (" << stamp << ", " << key.weekday << ", " 
       << key.hour << ", " << mean << ", " << std_dev << ")";
  LOG4CXX_DEBUG(log::logger, "DB: " << qtmp.str());

  ret = this->prepare(qtmp.str());

  ret = this->step();
  if (ret != SQLITE_DONE) {
    LOG4CXX_ERROR(log::logger, "error executing query: " << sqlite3_errmsg(db));
    return false;
  }

  ret = this->finalize();
  return true;
}

bool TSStore::query(const TimeTuple& key, double *qps, 
		    double *std_dev) {
  std::ostringstream qtmp;
  int ret;
  std::vector<double> m;
  std::vector<double> sd;

  time_t time_limit = system_timer->now() - ONE_MONTH_IN_SECONDS;
  qtmp << "select mean, std_dev from hourly_data where"
       << " wd = " << key.weekday << " and h = " << key.hour
       << " and stamp > " << time_limit;
  LOG4CXX_DEBUG(log::logger, "DB: " << qtmp.str());

  ret = prepare(qtmp.str());
  if (ret != SQLITE_OK)
    return false;

  /* get the query results */
  while ((ret = step()) == SQLITE_ROW) {
    m.push_back(sqlite3_column_double(stmt, 0));
    sd.push_back(sqlite3_column_double(stmt, 1));
  }

  if (ret != SQLITE_DONE) {
    //there was an error retrieving data..
    LOG4CXX_ERROR(log::logger, "error retrieving data: "
		  << sqlite3_errmsg(db));
    return false;
  }

  // DO STUFF WITH RESULTS! :)
  ret = finalize();

  if (m.size() > 0) {
    *qps = wd_mean(m);
    *std_dev = wd_std_dev(sd);
  } else {
    *qps = 0;
    *std_dev = 0;
  }
  return true;
}

bool TSStore::read_stats(const TimeTuple& target_key,
                         std::vector<QpsTuple>& out,
                         int hours) {
  out.clear();
  std::ostringstream qtmp;

  int target_stamp = target_key.weekday * 24 + target_key.hour;
  qtmp << "select h, wd, mean, std_dev, ("
       << target_stamp << " - wd * 24 + h) as s"
       << " from hourly_data "
       << " where (s >= 0 and s < " << hours
       << ") or (s <= " << (168 - hours) << " and s > -168)"
       << " order by wd asc, h asc";
  LOG4CXX_DEBUG(log::logger, "DB: " << qtmp.str());

  int ret = prepare(qtmp.str());
  if (ret != SQLITE_OK)
    return false;

  while ((ret = step()) == SQLITE_ROW) {
    QpsTuple q(sqlite3_column_double(stmt, 2),
               sqlite3_column_double(stmt, 3));
    q.stamp = TimeTuple(sqlite3_column_int(stmt, 0),
                        sqlite3_column_int(stmt, 1));
    out.push_back(q);
  }

  if (ret != SQLITE_DONE) {
    LOG4CXX_ERROR(log::logger, "error retrieving data: "
		  << sqlite3_errmsg(db));
    return false;
  }

  ret = finalize();
  return true;
}

double TSStore::wd_mean(std::vector<double>& wm) {
  double sum = 0;

  std::vector<double>::const_iterator i;

  for (i = wm.begin(); i != wm.end(); i++){
    sum += *i;  
  }

  return (sum / wm.size());
}

double TSStore::wd_std_dev(std::vector<double>& wstd) {
  double sum = 0;
  std::vector<double>::const_iterator i;

  for (i = wstd.begin(); i != wstd.end(); i++) {
    sum += pow(*i, 2);
  }

  return sqrt(sum) / wstd.size();
}

} // namespace dos
