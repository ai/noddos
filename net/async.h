// $Id$

#ifndef __net_async_H
#define __net_async_H 1

#include "common/log.h"
#include <event.h>
#include <iostream>
#include <cstring>
#include <errno.h>

namespace dos {
namespace net {

class Async
{
public:
  Async() 
    : evbase(static_cast<event_base *>(event_init()))
  {}

  void run() {
    if (0 != event_base_dispatch(evbase))
      LOG4CXX_ERROR(log::logger, "event.dispatch() error: " << strerror(errno));
  }

  void stop() {
    struct timeval right_now = {0, 0};
    event_base_loopexit(evbase, &right_now);
  }

  event_base* base() {
    return evbase;
  }

private:
  event_base *evbase;
};

class BasicEvent
  : public ::event
{
public:
  virtual void ready() {}

protected:
  BasicEvent()
    : async(0)
  {}

  static void wrapper(int fd, short ev, void *data) {
    BasicEvent *event = reinterpret_cast<BasicEvent *>(data);
    event->ready();
  }

  Async *async;
};

class Event
  : public BasicEvent
{
public:
  Event(int fd_)
  {
    event_set(this, fd_, EV_READ | EV_PERSIST, &wrapper,
	      reinterpret_cast<void *>(this));
  }

  int fd() const {
    return EVENT_FD(this);
  }

  void add(Async *async_) {
    async = async_;
    event_base_set(async->base(), this);
    event_add(this, 0);
  }
};

class Timer
  : public BasicEvent
{
public:
  Timer(int period_)
  {
    period.tv_sec = period_;
    period.tv_usec = 0;
    evtimer_set(this, &wrapper,
		reinterpret_cast<void *>(this));
  }

  void add(Async *async_) {
    async = async_;
    event_base_set(async->base(), this);
    evtimer_add(this, &period);
  }

  virtual void ready() {
    tick();
    event_add(this, &period);
  }

  virtual void tick() {}

protected:
  struct timeval period;
};

} // namespace net
} // namespace dos

#endif
