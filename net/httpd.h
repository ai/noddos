// $Id$

#ifndef __net_httpd_H
#define __net_httpd_H 1

#include <stdexcept>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/queue.h>
#include <evhttp.h>
#include "net/async.h"
#include "common/config.h"
#include "common/log.h"

namespace dos {
namespace net {

class HttpdCallback
{
public:
  HttpdCallback() {}
  virtual ~HttpdCallback() {}

  virtual void run(evhttp_request *req) = 0;
};

template <typename Class>
class HttpdMethodCallback
  : public HttpdCallback
{
public:
  typedef void (Class::*MethodType)(evhttp_request *);
  HttpdMethodCallback(Class *object, MethodType method)
    : object_(object), method_(method) {}
  ~HttpdMethodCallback() {}

  void run(evhttp_request *req) {
    (object_->*method_)(req);
  }

private:
  Class *object_;
  MethodType method_;
};

template <typename Class>
inline HttpdCallback *httpd_callback(Class *object, void (Class::*method)(evhttp_request *)) {
  return new HttpdMethodCallback<Class>(object, method);
}

class Httpd
{
public:
#if defined(HAVE_EVHTTP_NEW)
  Httpd(Async *async_, int port_)
    : httpd(evhttp_new(async_->base())),
      buffer(evbuffer_new()),
      async(async_),
      port(port_)
  {
    if (0 > evhttp_bind_socket(httpd, "0.0.0.0", port)) {
      LOG4CXX_ERROR(log::logger, "could not bind to port " << port_);
      throw(std::runtime_error("bind() failed"));
    }
    add_handler("/style.css", httpd_callback(this, &Httpd::handle_style_css));
    LOG4CXX_INFO(log::logger, "started httpd on port " << port_);
  }
#else
  Httpd(Async *async_, int port_)
    : httpd(evhttp_start("0.0.0.0", port_)),
      buffer(evbuffer_new()),
      async(async_),
      port(port_)
  {
    add_handler("/style.css", httpd_callback(this, &Httpd::handle_style_css));
    LOG4CXX_INFO(log::logger, "started httpd on port " << port_);
  }
#endif

  ~Httpd() {
    evbuffer_free(buffer);
    evhttp_free(httpd);
  }

  void add_handler(const char *url, HttpdCallback *cb) {
    evhttp_set_cb(httpd, url, &Httpd::wrapper, 
                  reinterpret_cast<void *>(cb));
  }

  void send_reply(evhttp_request *req, int status, 
                  const char *status_str, const char *reply,
                  int reply_len) {
    evbuffer_expand(buffer, reply_len);
    evbuffer_add(buffer, (void *)reply, reply_len);
    evhttp_send_reply(req, status, status_str, buffer);
  }

  void set_header(evhttp_request *req, const char *hdr,
                  const char *value) {
    evhttp_add_header(req->output_headers, hdr, value);
  }

  virtual void handle_style_css(evhttp_request *req);

protected:
  static void wrapper(evhttp_request *req, void *data) {
    HttpdCallback *cb = reinterpret_cast<HttpdCallback *>(data);
    cb->run(req);
  }

  Async *async;
  evhttp *httpd;
  evbuffer *buffer;
  int port;
};


} // namespace net
} // namespace dos

#endif
