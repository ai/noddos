// $Id$

#include "common/log.h"
#include "net/async.h"
#include <sys/socket.h>
#include <unistd.h>
#include <gtest/gtest.h>

using namespace std;
using namespace dos::net;

namespace {

struct timer_fired_check 
  : public Timer
{
  int limit, fired;
  timer_fired_check(int period, int howmany=1) 
    : Timer(period), fired(0), limit(howmany)
  {}
  virtual void tick() {
    if (++fired >= limit)
      async->stop();
  }
};

TEST(AsyncTest, TimerFiresCorrectly) {
  Async a;
  timer_fired_check tfc(1, 1);
  tfc.add(&a);
  a.run();
  ASSERT_EQ(1, tfc.fired);
}

TEST(AsyncTest, TimerFiresRepeatedly) {
  Async a;
  timer_fired_check tfc2(1, 2);
  tfc2.add(&a);
  a.run();
  ASSERT_EQ(2, tfc2.fired);
}

} // namespace

int main(int argc, char **argv) {
  dos::log::init();
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
