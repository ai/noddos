// $Id$

#include "common/log.h"
#include "net/httpd.h"
#include <stdexcept>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <string>
#include <gtest/gtest.h>

using namespace std;
using namespace dos::net;

namespace {

#define TEST_PKT  "somefunkymessage"

// Make an http request, low-level style
struct httpreq {
  httpreq(const char *addr_, int port_)
    : addr(addr_), port(port_)
  {}

  bool get(const char *url) {
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0) {
      cerr << "socket() failed: " << strerror(errno) << "\n";
      return false;
    }

    struct sockaddr_in connect_addr;
    connect_addr.sin_family = AF_INET;
    inet_aton(addr, &connect_addr.sin_addr);
    connect_addr.sin_port = htons(port);
    if (connect(sock, (struct sockaddr *)&connect_addr, sizeof(connect_addr)) < 0) {
      cerr << "connect() failed: " << strerror(errno) << "\n";
      return false;
    }

    char request[256];
    snprintf(request, 255, "GET %s HTTP/1.0\r\nAccept: *\r\n\r\n", url);
    int req_len = strlen(request);
    int n = send(sock, request, req_len, 0);
    if (n != req_len) {
      cerr << "send() failed: " << strerror(errno) << "\n";
      return false;
    }

    // libevent 1.3 misbehaves if we shutdown our end prematurely
    //shutdown(sock, SHUT_WR);

    int max_response_size = 4096;
    char *response_buf = (char *)malloc(max_response_size);
    n = recv(sock, response_buf, max_response_size, 0);
    if (n < 0) {
      cerr << "recv() failed: " << strerror(errno) << "\n";
      return false;
    }
    string httpresp(response_buf, n);
    if (httpresp.substr(0, 6) != "HTTP/1") {
      cerr << "response is not HTTP: " << httpresp.substr(0, 64) << "\n";
      return false;
    }
    string status_str(httpresp.substr(9, 3));
    status = atoi(status_str.c_str());
    n = httpresp.find("\r\n\r\n");
    if (n > 0)
      response = httpresp.substr(n + 4, -1);

    free(response_buf);
    close(sock);
    return true;
  }

  const char *addr;
  int port;
  string response;
  int status;
};

// make a request in a different thread and saves the response
// the request is delayed by 1 second.
struct RequestThread {
  RequestThread(int port_, const char *url_)
    : request("127.0.0.1", port_), url(url_)
  {
    pthread_create(&thr, 0, &RequestThread::run, this);
  }

  static void* run(void *dataptr) {
    RequestThread *rqt = reinterpret_cast<RequestThread *>(dataptr);
    char *pkt = TEST_PKT;
    sleep(1);
    return (void *)(rqt->request.get(rqt->url) ? 0 : 1);
  }

  void join() {
    void *status;
    pthread_join(thr, &status);
  }

  pthread_t thr;
  const char *url;
  httpreq request;
};

// watchdog timeout that stops the async runner after two seconds
class TestTimeout
  : public Timer
{
public:
  TestTimeout() : Timer(2) {}

  void tick() {
    async->stop();
  }
};

// a test http server
class TestHttpd
  : public Httpd
{
public:
  TestHttpd(Async *async_, int port_)
    : Httpd(async_, port_)
  {
    add_handler("/", httpd_callback(this, &TestHttpd::handle_root));
  }

  void handle_root(evhttp_request *req) {
    send_reply(req, 200, "OK", TEST_PKT, strlen(TEST_PKT));
    async->stop();
  }
};

TEST(HttpdTest, HttpdFullTest) {
  int test_port = 60222;
  Async async;
  TestTimeout timeout;
  timeout.add(&async);
  TestHttpd h(&async, test_port);
  RequestThread req_th(test_port, "/");
  async.run();
  req_th.join();

  EXPECT_EQ(200, req_th.request.status);
  EXPECT_EQ(TEST_PKT, req_th.request.response);
}

TEST(HttpdTest, Httpd404Test) {
  int test_port = 60222;
  Async async;
  TestTimeout timeout;
  timeout.add(&async);
  TestHttpd h(&async, test_port);
  RequestThread req_th(test_port, "/nonexisting.html");
  async.run();
  req_th.join();

  EXPECT_EQ(404, req_th.request.status);
}

} // namespace

int main(int argc, char **argv) {
  dos::log::init();
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
