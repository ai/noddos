// $Id$

#include "common/log.h"
#include "net/net.h"
#include <sys/socket.h>
#include <unistd.h>
#include <pthread.h>
#include <string>
#include <gtest/gtest.h>

using namespace std;
using namespace dos::net;

namespace {

#define TEST_PKT  "somefunkymessage"

struct PacketSendThread {
  PacketSendThread(const char *server_, int port_)
    : sender(server_, port_)
  {
    pthread_create(&thr, 0, &PacketSendThread::run, this);
  }

  static void* run(void *dataptr) {
    PacketSendThread *pst = reinterpret_cast<PacketSendThread *>(dataptr);
    char *pkt = TEST_PKT;
    sleep(1);
    pst->sender.send(pkt, strlen(pkt));
    return 0;
  }

  void join() {
    void *status;
    pthread_join(thr, &status);
  }

  pthread_t thr;
  Sender sender;
};

class TestListener
  : public Listener
{
public:
  TestListener(int port_)
    : Listener(port_)
  {}

  virtual void handle_message(char *buf, int n) {
    if (buf)
      received = string(buf, n);
    async->stop();
  }

  string received;
};

TEST(NetTest, ListenerFullTest) {
  int test_port = 60222;
  Async async;
  TestListener listener(test_port);
  listener.add(&async);
  PacketSendThread sender("127.0.0.1", test_port);
  async.run();
  ASSERT_EQ(TEST_PKT, listener.received);
}

} // namespace

int main(int argc, char **argv) {
  dos::log::init();
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
