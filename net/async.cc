// $Id$

#include "async.h"
#include <event.h>

namespace dos {

static void evcb(int fd, short int evtype, void *data) {
  Async::callback_t cb = *(static_cast<Async::callback_t *>(data));
  cb();
}

Async::Async()
  : evbase(event_base_new())
{}

void Async::add_listener(int fd, callback_t cb) {
  event *ev = new event;
  event_set(ev, fd, EV_PERSIST | EV_READ, evcb, &cb);
  event_base_set(evbase, ev);
  event_add(ev, NULL);
  events.push_back(ev);
}

void Async::add_timer(int period, callback_t cb) {
  struct timeval period_tv = { period, 0 };
  event *ev = new event;
  event_set(ev, -1, EV_PERSIST, evcb, &cb);
  event_base_set(evbase, ev);
  event_add(ev, &period_tv);
  events.push_back(ev);
}

void Async::run() {
  event_base_dispatch(evbase);
}

void Async::stop() {
  event_base_loopbreak(evbase);
}

} // namespace dos
