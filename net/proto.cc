// $Id$

#include "proto.h"
#include "common/log.h"
#include <iostream>

namespace dos {
namespace net {

static const int HEADER_SIZE = 4;

char *RawMessage::decode(int *out_len)
{
  if (size < HEADER_SIZE) {
    errmsg = "Short packet";
    LOG4CXX_ERROR(log::logger, errmsg << " (size=" << size << ")");
    return NULL;
  }
  uint16_t len = ntohs(*(uint16_t *)buffer);
  if (len != (size - HEADER_SIZE)) {
    errmsg = "Size mismatch";
    LOG4CXX_ERROR(log::logger, errmsg << " (size=" << size << ", len=" << len << ")");
    return NULL;
  }
  uint16_t checksum = ntohs(*(uint16_t *)(buffer + 2));
  char *payload = buffer + HEADER_SIZE;
  if (checksum != compute_checksum(payload, len)) {
    errmsg = "Checksum error";
    LOG4CXX_ERROR(log::logger, errmsg << " (cksum=" << checksum << ", vrfy="
		  << compute_checksum(payload, len) << ")");
    return NULL;
  }

  *out_len = len;
  return payload;
}

uint16_t RawMessage::compute_checksum(char *payload, int len) {
  // Just a proof-of-concept implementation.  Replace with proper CRC,
  // or even HMAC-based integrity checking at some point.
  int accum = 0;
  while (len--)
    accum += *payload++;
  return (accum % 0xffff);
}

bool RawMessage::encode(char *payload, int len)
{
  if (len > (size - HEADER_SIZE)) {
    errmsg = "Message too large";
    return false;
  }
  *(uint16_t *)buffer = htons(len);
  *(uint16_t *)(buffer + 2) = htons(compute_checksum(payload, len));
  memcpy(buffer + HEADER_SIZE, payload, len);
  size = len + HEADER_SIZE;
  return true;
}

} // namespace net
} // namespace dos
