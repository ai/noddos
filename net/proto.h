// $Id$

#ifndef __net_proto_H
#define __net_proto_H 1

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "noddos.pb.h"

namespace dos {
namespace net {

/**
 * On-wire message wrapper.
 *
 * Provides integrity checking of the payload. In the future we may
 * add authentication hooks as well. 
 *
 * The caller must provide a (static) buffer for decode/encode
 * operations. The buffer must be large enough for the largest payload
 * that you want to support.
 */
class RawMessage {
public:
  RawMessage(char *buffer_, int size_)
    : buffer(buffer_), size(size_), errmsg(NULL)
  {}

  char *decode(int *out_len);

  bool encode(char *payload, int len);

  const char *errmsg;
  char *buffer;
  int size;

private:
  static uint16_t compute_checksum(char *payload, int len);
};

} // namespace net
} // namespace dos

#endif
