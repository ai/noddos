// $Id$ -*-c++-*-

#ifndef __net_dist_H
#define __net_dist_H 1

#include <string>
#include "noddos.pb.h"
#include "net.h"

namespace dos {
namespace net {

class RpcProxy
  : public Sender
{
public:
  RpcProxy(const char *server_ip, int server_port)
    : Sender(server_ip, server_port)
  {}

  bool send_message(google::protobuf::Message *msg);
};

class MasterProxy
  : public RpcProxy
{
public:
  MasterProxy(const char *server_ip, int server_port);

  bool send_blacklist(int node_id, struct in_addr ip);

  bool send_qps(int node_id, double value);

  std::string master_addr;
};

class ProbeProxy
  : public RpcProxy
{
public:
  ProbeProxy(const char *server_ip, int server_port)
    : RpcProxy(server_ip, server_port)
  {}

  bool send_status(const std::string& status);
};

class MasterImpl
  : public Listener
{
public:
  MasterImpl(int port_)
    : Listener(port_)
  {}

  virtual void handle_message(char *buf, int n);
  virtual void handle_qps(int node_id, const Qps& qps) {}
  virtual void handle_blacklist(int node_id, const Ip& ip) {}
};

class ProbeImpl
  : public Listener
{
public:
  ProbeImpl()
  {}

  ProbeImpl(int port_)
    : Listener(port_)
  {}

  virtual void handle_message(char *buf, int n);
  virtual void handle_status(const std::string& status) {}
};

} // namespace net
} // namespace dos

#endif
