// $Id$

#include <iostream>
#include <sstream>
#include <stdio.h>
#include "httpd.h"

namespace dos {
namespace net {

static const char *CSS =
  "body {\n"
  " background: white;\n"
  " color: #222;\n"
  " font-family: serif;\n"
  " margin: 0;\n"
  " padding: 1ex 4ex;\n"
  " border-top: 6px solid #555;\n"
  "}\n"
  "h1,h2,h3 {\n"
  " font-family: arial;\n"
  " letter-spacing: -0.1em;\n"
  "}\n"
  ".d thead {\n"
  " border-bottom: 1px solid black;\n"
  "}\n"
  ".big {\n"
  " font-size: 160%;\n"
  "}\n";

void Httpd::handle_style_css(evhttp_request *req) {
  char hlen[8];
  sprintf(hlen, "%d", strlen(CSS));
  set_header(req, "Content-Length", hlen);
  set_header(req, "Content-Type", "text/css");
  send_reply(req, 200, "OK", CSS, strlen(CSS));
}

} // namespace net
} // namespace dos

