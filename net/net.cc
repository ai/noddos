// $Id$

#include "proto.h"
#include "net.h"
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <iostream>

namespace dos {
namespace net {

static const int DATA_SIZE = 8192;

static int init_socket(int port=0) throw(SocketError)
{
  int sock = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock < 0)
    throw(new SocketError("socket()"));

  if (port > 0) {
    struct sockaddr_in bind_addr;
    bind_addr.sin_family = AF_INET;
    bind_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    bind_addr.sin_port = htons(port);
    if (bind(sock, (struct sockaddr *)&bind_addr, sizeof(bind_addr)) < 0)
      throw(new SocketError("bind()"));
  }

  return sock;
}

Sender::Sender(const char *server_ip, int server_port)
  throw(SocketError)
{
  server_addr.sin_family = AF_INET;
  inet_aton(server_ip, &server_addr.sin_addr);
  server_addr.sin_port = htons(server_port);
  sock = init_socket();
  buffer = (char *)malloc(DATA_SIZE);
}

Sender::~Sender()
{
  close(sock);
  free(buffer);
}

bool Sender::send(char *buf, int n)
{
  RawMessage msg(buffer, DATA_SIZE);
  if (!msg.encode(buf, n))
    return false;
  while (true) {
    size_t r = sendto(sock, buffer, msg.size, 0,
                      (struct sockaddr *)&server_addr,
                      sizeof(struct sockaddr_in));
    if (r >= 0)
      return true;
    if (errno != EINTR) {
      std::cerr << "Could not send message: " << strerror(errno) << std::endl;
      return false;
    }
  }
}

/*********************************************************************/

Listener::Listener(int port_)
  throw(SocketError)
  : port(port_),
    Event(init_socket(port_)),
    data((char *)malloc(DATA_SIZE))
{
}

Listener::~Listener()
{
  close(fd());
  free(data);
}

void Listener::ready()
{
  struct sockaddr_in peer_addr;
  socklen_t peer_len = sizeof(peer_addr);
  size_t n;

  n = recvfrom(fd(), data, DATA_SIZE, 0,
               (struct sockaddr *)&peer_addr, &peer_len);
  if (n < 0) {
    if (errno != EINTR)
      std::cerr << "Error in recvfrom(): " << strerror(errno) << std::endl;
  } else {
    strcpy(peer, inet_ntoa(peer_addr.sin_addr));
    handle_raw_message(data, n);
  }
}

void Listener::handle_raw_message(char *buf, int n)
{
  RawMessage msg(buf, n);
  int payload_size;
  char *payload = msg.decode(&payload_size);
  if (!payload) {
    std::cerr << "Error decoding packet: " << msg.errmsg << std::endl;
    return;
  }

  handle_message(payload, payload_size);
}

} // namespace net
} // namespace dos
