// $Id$

#include "dist.h"
#include <iostream>
#include <sstream>

namespace dos {
namespace net {

bool RpcProxy::send_message(google::protobuf::Message *msg)
{
  bool result = false;
  int sz = msg->ByteSize();
  char *buf = (char *)malloc(sz);
  if (msg->SerializeToArray(buf, sz)
      && send(buf, sz))
    result = true;
  free(buf);
  return result;
}

MasterProxy::MasterProxy(const char *server_ip, int server_port)
  : RpcProxy(server_ip, server_port)
{
  std::ostringstream sbuf;
  sbuf << server_ip << ":" << server_port;
  master_addr = sbuf.str();
}

bool MasterProxy::send_blacklist(int node_id, struct in_addr ip)
{
  ProbeMessage msg;
  msg.set_type(ProbeMessage::BLACKLIST);
  msg.set_node_id(node_id);
  msg.mutable_blacklist()->set_ip(inet_ntoa(ip));
  return send_message(&msg);
}

bool MasterProxy::send_qps(int node_id, double value)
{
  ProbeMessage msg;
  msg.set_type(ProbeMessage::QPS);
  msg.set_node_id(node_id);
  msg.mutable_qps()->set_qps(value);
  return send_message(&msg);
}

bool ProbeProxy::send_status(const std::string& status)
{
  MasterMessage msg;
  msg.set_type(MasterMessage::STATUS);
  msg.set_status(status);
  return send_message(&msg);
}

void MasterImpl::handle_message(char *buf, int n)
{
  // decode the protocol buffer
  ProbeMessage msg;
  if (!msg.ParseFromArray(buf, n)) {
    LOG4CXX_ERROR(log::logger, peer << ": error decoding payload");
    return;
  }
  LOG4CXX_DEBUG(log::logger, peer << "received message, type=" 
		<< msg.type() << " node_id=" << msg.node_id());
  switch (msg.type()) {
  case ProbeMessage::QPS:
    handle_qps(msg.node_id(), msg.qps());
    break;
  case ProbeMessage::BLACKLIST:
    handle_blacklist(msg.node_id(), msg.blacklist());
    break;
  }
}

void ProbeImpl::handle_message(char *buf, int n)
{
  // decode the protocol buffer
  MasterMessage msg;
  if (!msg.ParseFromArray(buf, n)) {
    LOG4CXX_ERROR(log::logger, peer << ": error decoding payload");
    return;
  }
  LOG4CXX_DEBUG(log::logger, peer << ": received message, type=" 
		<< msg.type());
  switch (msg.type()) {
  case MasterMessage::STATUS:
    handle_status(msg.status());
    break;
  }
}

} // namespace net
} // namespace dos
