// $Id$

#ifndef __net_H
#define __net_H 1

#include <stdexcept>
#include <sstream>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include "net/async.h"

namespace dos {
namespace net {

struct SocketError 
  : public std::runtime_error
{
  SocketError(const char *where) throw()
    : std::runtime_error(make_error(where))
  {}

  static std::string make_error(const char *where) {
    std::ostringstream errbuf;
    errbuf << where << ": " << strerror(errno);
    return errbuf.str();
  }
};

class Listener
  : public Event
{
public:
  Listener()
    : Event(-1)
  {}

  Listener(int port_) throw(SocketError);

  virtual ~Listener();

  virtual void ready();

  void handle_raw_message(char *buf, int n);

  virtual void handle_message(char *buf, int n) {}

protected:
  int port;
  char *data;
  char peer[16];
};

class Sender
{
public:
  Sender(const char *server_ip, int server_port)
    throw(SocketError);

  virtual ~Sender();

  bool send(char *buf, int n);

protected:
  char *buffer;
  int sock;
  struct sockaddr_in server_addr;
};

} // namespace net
} // namespace dos

#endif
