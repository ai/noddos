// $Id$

#ifndef __acl_H
#define __acl_H 1

#include <exception>
#include <vector>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

namespace dos {

class AclParseError
  : public std::exception
{
  virtual const char* what() const throw() {
    return "IP parse error";
  }
};

class Acl {
public:
  Acl(const char *cidr);

  bool match(in_addr addr) {
    unsigned long s = addr.s_addr & mask.s_addr;
    return (s == net.s_addr);
  }

private:
  in_addr net, mask;

};

class AclMatcher {
public:
  AclMatcher() {}

  void add(const char *cidr) {
    acls.push_back(Acl(cidr));
  }

  bool match(in_addr addr) {
    std::vector<Acl>::iterator i;
    for (i = acls.begin(); i != acls.end(); i++)
      if ((*i).match(addr))
        return true;
    return false;
  }

private:
  std::vector<Acl> acls;

};

} // namespace

#endif
