// $Id$ -*-c++-*-

#ifndef __master_blacklist_H
#define __master_blacklist_H 1

#include <map>
#include <string>
#include <vector>
#include "common/timer.h"

namespace dos {

class Blacklist 
{
public:
  enum { EXPIRE_TIME=86400 };

  typedef std::map<std::string, time_t> blacklist_map;

  Blacklist() {}

  void add(const std::string& ip) {
    time_t now = system_timer->now();
    blacklist_map::iterator i = bl.find(ip);
    if (i == bl.end()) {
      bl.insert(std::make_pair(ip, now));
      disable(ip);
    } else {
      (*i).second = now;
    }
  }

  void expire() {
    time_t now = system_timer->now();
    blacklist_map::iterator i = bl.begin();
    while (i != bl.end()) {
      if ((now - (*i).second) > EXPIRE_TIME) {
        enable((*i).first);
        bl.erase(i++);
      } else {
        ++i;
      }
    }
  }

  std::vector<std::string> list() {
    std::vector<std::string> ips;
    for (blacklist_map::const_iterator i = bl.begin(); i != bl.end(); i++)
      ips.push_back((*i).first);
    return ips;
  }

  virtual void enable(const std::string& ip) {}

  virtual void disable(const std::string& ip) {}

private:
  blacklist_map bl;
};

} // namespace dos

#endif
