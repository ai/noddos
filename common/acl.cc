// $Id$

#include <stdio.h>
#include <string.h>
#include "acl.h"

namespace dos {

Acl::Acl(const char *cidr) {
  char cidr_net[32];
  int cidr_netsize = 32;
  int n = sscanf(cidr, "%[^/]/%d", cidr_net, &cidr_netsize);
  if (n == 0)
    throw AclParseError();
  if (n == 1)
    strncpy(cidr_net, cidr, sizeof(cidr_net) - 1);
  if (!inet_aton(cidr_net, &net))
    throw AclParseError();
  unsigned long lmask = htonl(0xffffffff << (32 - cidr_netsize));
  net.s_addr &= lmask;
  mask.s_addr = lmask;
}

} // namespace
