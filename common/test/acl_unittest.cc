// $Id$

#include <iostream>
#include <gtest/gtest.h>
#include "common/log.h"
#include "common/acl.h"

using namespace std;
using namespace dos;

namespace {

TEST(AclTest, MatchesValidIpAddresses) {
  struct in_addr addr;
  Acl acl("1.2.3.4");
  inet_aton("1.2.3.4", &addr);
  EXPECT_TRUE(acl.match(addr));
  inet_aton("2.3.4.5", &addr);
  EXPECT_FALSE(acl.match(addr));
}

TEST(AclTest, MatchesValidIpRange) {
  const char *ranges[] = {
    "10.0.0.0/8", "10.99.99.99/8", "10.0.0/8"
  };
  for (int i = 0; i < 3; i++) {
    const char *range = ranges[i];
    struct in_addr addr;
    Acl acl(range);
    inet_aton("10.2.3.4", &addr);
    EXPECT_TRUE(acl.match(addr)) 
      << "Range " << range << " failed match for 10.2.3.4";
    inet_aton("2.3.4.5", &addr);
    EXPECT_FALSE(acl.match(addr))
      << "Range " << range << " failed non-match for 2.3.4.5";
  }
}

TEST(AclTest, FailsOnWrongAddresses) {
  ASSERT_THROW(Acl(""), AclParseError);
  ASSERT_THROW(Acl("not.an.address"), AclParseError);
  ASSERT_THROW(Acl("/24"), AclParseError);
  ASSERT_THROW(Acl("1.2.3.4.5"), AclParseError);
}

TEST(AclMatcherTest, SanityCheck) {
  AclMatcher m;
  m.add("10.0.0.0/8");
  m.add("1.2.3.4");

  struct in_addr addr;
  inet_aton("1.2.3.4", &addr);
  EXPECT_TRUE(m.match(addr));

  inet_aton("10.0.0.1", &addr);
  EXPECT_TRUE(m.match(addr));

  inet_aton("2.3.4.5", &addr);
  EXPECT_FALSE(m.match(addr));
}

} // namespace

int main(int argc, char **argv) {
  dos::log::init();
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
