// $Id$

#include <iostream>
#include <sstream>
#include <gtest/gtest.h>
#include "common/ntm.h"
#include "common/test/test_timer.h"

using namespace std;
using namespace dos;

namespace {

typedef NTM<const char *, __gnu_cxx::hash<const char *> > TestNTM;

static TestTimer *mock_timer = new TestTimer();

class ControlledTimerEnvironment
  : public testing::Environment
{
public:
  virtual void SetUp() {
    system_timer = mock_timer;
  }
};

string ntm_state_to_str(TestNTM *ntmp, const char *key) {
  ostringstream out;
  int count=-1;
  time_t stamp=-1;
  if (ntmp->get_state(key, &count, &stamp)) {
    out << "<ntm:count=" << count << ",stamp=" << stamp << ">";
  } else {
    out << "<ntm:UNDEF>";
  }
  return out.str();
}

TEST(NTMTest, SanityCheck) {
  TestNTM n;
  int res = n.hit("TEST1");
  ASSERT_EQ(TestNTM::ALLOW, res);
}

TEST(NTMTest, RateLimit) {
  mock_timer->set(0);
  TestNTM n(1, 2);              // 2 qps
  for (int i=0; i<3; i++) {
    mock_timer->set(i);
    EXPECT_EQ(TestNTM::ALLOW, n.hit("TEST"))
      << "ratelimit fail, round: " << i
      << ", state: " << ntm_state_to_str(&n, "TEST");
    EXPECT_EQ(TestNTM::ALLOW, n.hit("TEST"))
      << "ratelimit fail, round: " << i
      << ", state: " << ntm_state_to_str(&n, "TEST");
    EXPECT_EQ(TestNTM::DENY, n.hit("TEST"))
      << "ratelimit fail, round: " << i
      << ", state: " << ntm_state_to_str(&n, "TEST");
  }
}

// Count the number of times that ntm returns ALLOW given
// a target rate.
TEST(NTMTest, FiresTheExpectedNumberOfTimes) {
  mock_timer->set(0);
  TestNTM n(10, 20);            // 2 qps over 10 seconds
  int count=1000;
  int ok_count=0;
  int time=0;

  // simulate a 4qps stream
  for (int i=0; i<count; i++) {
    if (i % 4 == 0)
      mock_timer->set(time++);
    if (n.hit("TEST") == TestNTM::ALLOW)
      ok_count++;
  }

  // we expect  time*2 ok queries, since 2 qps is the target rate
  int exp_ok = time * 2;
  EXPECT_EQ(exp_ok, ok_count)
    << ntm_state_to_str(&n, "TEST");
}

// Quick test to check our ability to extract the current
// per-key qps value from the ntm internals.
TEST(NTMTest, Limit2QPS) {
  mock_timer->set(0);
  TestNTM n(1, 2);              // 2 qps
  for (int i=0; i<4; i++)
    n.hit("TEST");
  int count=-1;
  time_t stamp=-1;
  EXPECT_TRUE(n.get_state("TEST", &count, &stamp));
  int qps = count / (1 - stamp);
  EXPECT_EQ(4, qps) << "incorrect qps, state: " 
                    << ntm_state_to_str(&n, "TEST");
}

// similar to the above, but test different time resolutions
TEST(NTMTest, Limit2QPSExtensive) {
  for (int base=1; base<=32; base *= 2) {
    mock_timer->set(0);
    TestNTM n(base, 2*base);              // 2 qps
 
    // provide a constant 4qps stream for some time
    int time=0;
    for (int i=0; i<128; i++) {
      if (i % 4 == 0)
        mock_timer->set(time++);
      n.hit("TEST");
    }

    int count=-1;
    time_t stamp=-1;
    EXPECT_TRUE(n.get_state("TEST", &count, &stamp));
    int qps = count / (time - stamp);
    EXPECT_EQ(4, qps) << "incorrect qps, base: " << base 
                      << "state: " << ntm_state_to_str(&n, "TEST");
  }
}

} // namespace

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  testing::AddGlobalTestEnvironment(new ControlledTimerEnvironment);
  return RUN_ALL_TESTS();
}
