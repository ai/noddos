#include "common/timer.h"

namespace dos {

// A Timer class that returns the time that we want
class TestTimer
  : public Timer
{
public:
  virtual time_t now() {
    return cur_value;
  }

  void set(time_t new_value) {
    cur_value = new_value;
  }

private:
  time_t cur_value;
};

} // namespace dos
