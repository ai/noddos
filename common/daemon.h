// $Id$

#ifndef __common_daemon_H
#define __common_daemon_H 1

#include <sys/types.h>

namespace dos {
namespace util {

  void daemonize(uid_t uid=0, const char *pid_file=NULL);

} // namespace util
} // namespace dos

#endif
