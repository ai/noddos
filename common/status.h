// $Id$ -*-c++-*-

#ifndef __common_status_H
#define __common_status_H 1

namespace dos {

class Status
{
public:
  enum { OK=0, ATTACK=1 };

  Status(int s)
    : status_(s)
  {}

  Status(const std::string& s) {
    if (s == "ok")
      status_ = OK;
    else
      status_ = ATTACK;
  }

  const char *str() const {
    switch(status_) {
    case OK:
      return "ok";
    case ATTACK:
      return "attack";
    }
  }

  const int value() const {
    return status_;
  }

  bool operator !=(const Status& s) {
    return (status_ != s.status_);
  }

private:
  int status_;

};

} // namespace dos

#endif


