// $Id$

#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

namespace dos {
namespace util {

static bool daemon_timeout_expired = false;

static void daemon_timeout(int signum) {
  daemon_timeout_expired = true;
}

static void sigusr1_handler(int signum) {
}

static void write_pid_file(const char *file) {
  std::ofstream out(file, std::ofstream::out);
  out << getpid() << "\n";
  out.close();
}

void daemonize(uid_t uid, const char *pid_file) {
  sigset_t mask, old_mask;
  pid_t parent = getpid();

  signal(SIGUSR1, sigusr1_handler);

  // Block all signals
  sigfillset(&mask);
  sigprocmask(SIG_BLOCK, &mask, &old_mask);

  pid_t pid = fork();
  if (pid == 0) {
    sigprocmask(SIG_UNBLOCK, &mask, &old_mask);

    if (pid_file)
      write_pid_file(pid_file);

    int nullfd = open("/dev/null", O_RDWR);
    dup2(nullfd, 0);
    dup2(nullfd, 1);
    dup2(nullfd, 2);

    setsid();
    if (uid != 0)
      setuid(uid);
 
    // Signal the parent that we're ready, and let the program continue.
    kill(parent, SIGUSR1);
    return;

  } else {

    // Wait for a SIGUSR1, with timeout.
    signal(SIGALRM, daemon_timeout);
    sigdelset(&mask, SIGUSR1);
    alarm(10);
    sigsuspend(&mask);
    sigprocmask(SIG_SETMASK, &old_mask, &mask);
    if (daemon_timeout_expired) {
      std::cerr << "Error: background process could not be started\n";
      exit(1);
    } else {
      exit(0);
    }

  }
}

} // namespace util
} // namespace dos
