// $Id$

#ifndef __common_log_H
#define __common_log_H 1

#include <log4cxx/logger.h>

using namespace log4cxx;

namespace dos {
namespace log {

extern LoggerPtr logger;

void init(bool syslog=false);

} // namespace log
} // namespace dos

#endif
