// $Id$

#include <cstring>
#include <stdlib.h>
#include "util.h"

using namespace std;

namespace dos {
namespace util {

bool parse_addr(const string& addr, char *host, int *port) {
  if (addr.find(':') < 0)
    return false;
  string host_str = addr.substr(0, addr.find(':'));
  string addr_str = addr.substr(addr.find(':') + 1, -1);
  strcpy(host, host_str.c_str());
  *port = atoi(addr_str.c_str());
  return true;
}

} // namespace util
} // namespace dos

