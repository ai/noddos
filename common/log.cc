// $Id$

#include <syslog.h>
#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/patternlayout.h>
#include <log4cxx/basicconfigurator.h>
#include <log4cxx/net/syslogappender.h>
#include "log.h"

using namespace log4cxx;
using namespace log4cxx::net;

namespace dos {
namespace log {

static const char *PATTERN = "%p - %m";

LoggerPtr logger(Logger::getLogger("noddos"));

void init(bool syslog) {
  if (syslog) {
    //LogManager::getLoggerRepository()->setConfigured(true);
    LoggerPtr root = Logger::getRootLogger();
    LayoutPtr layout(new PatternLayout(PATTERN));
    AppenderPtr appender(new SyslogAppender(layout, LOG_LOCAL1));
    root->addAppender(appender);
  } else {
    BasicConfigurator::configure();
  }
}

} // namespace log
} // namespace dos
