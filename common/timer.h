// $Id$
// isolate the system timer, for easier testing

#ifndef __timer_H
#define __timer_H 1

#include <time.h>

namespace dos {

// A Timer class that simply returns the system time
class Timer
{
public:
  virtual time_t now() {
    return time(NULL);
  }
};

extern Timer *system_timer;

} // namespace dos

#endif
