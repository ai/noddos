// $Id$

#ifndef __common_util_H
#define __common_util_H 1

#include <algorithm>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

namespace dos {
namespace util {

// 'host' must be allocated by the caller
bool parse_addr(const std::string& in, char *host, int *port);

template<class T>
void read_file(const char *filename, T callback) {
  std::ifstream in(filename, std::ifstream::in);
  int lineno = 0;
  while (in.good()) {
    std::string line;
    std::getline(in, line);
    ++lineno;
    if (in.eof())
      break;
    if (line.size() == 0 || line[0] == '#')
      continue;
    std::istringstream line_s(line);
    std::vector<std::string> tokens;
    std::copy(std::istream_iterator<std::string>(line_s),
              std::istream_iterator<std::string>(),
              std::back_inserter<std::vector<std::string> >(tokens));
    if (!callback(tokens))
      std::cerr << "Syntax error at line " << lineno << "\n";
  }
  in.close();
}

inline int to_int(const std::string& s) {
  std::istringstream st(s);
  int i = 0;
  st >> i;
  return i;
}

} // namespace util
} // namespace dos

#endif
