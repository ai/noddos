// $Id$
// Name-based Timestamp Map

#ifndef __ntm_H
#define __ntm_H 1

#include "timer.h"
#ifdef HAVE_CONFIG_H
#include "config.h"
#ifdef HAVE_HASH_MAP
#include <hash_map>
#else
#include <ext/hash_map>
#endif // HAVE_HASH_MAP
#endif // HAVE_CONFIG_H

namespace dos {

template<typename T>
struct NTMNode
{
  NTMNode() 
    : count(0)
  {}

  NTMNode(const T& _key, time_t _ts)
    : key(_key), timestamp(_ts), count(0)
  {}

  bool operator==(const NTMNode<T>& b) {
    return (b.key == key);
  }

  T key;
  time_t timestamp;
  long count;
};

template<typename T, class HASH, class EQ=std::equal_to<T> >
class NTM 
{
public:
  enum { CLEANUP_TIME=900 };

  enum { DENY=0, ALLOW };

  typedef NTMNode<T> *NTMNodePtr;
  typedef __gnu_cxx::hash_map<T, NTMNodePtr, HASH, EQ> NTMMap;
  typedef typename NTMMap::iterator iterator;

  /** Constructor.
   *
   * @param _interval threshold period (in seconds)
   * @param _count threshold count
   */
  NTM(int interval_=60, int count_=100) 
    : threshold_interval(interval_),
      threshold_count(count_),
      last_cleanup(system_timer->now())
  {}

  /** Reset thresholds.
   *
   * @param _interval threshold period (in seconds)
   * @param _count threshold count
   */
  void set_threshold(int interval_, int count_) {
    threshold_interval = interval_;
    threshold_count = count_;
  }

  /** Report a hit.
   *
   * @param key the key to register the hit for
   * @returns one of ALLOW, DENY according to the currently configured 
   * thresholds.
   */
  int hit(const T& key) {
    time_t now = system_timer->now();
    if (now - last_cleanup > CLEANUP_TIME)
      cleanup(CLEANUP_TIME * 2);

    NTMNodePtr node;
    iterator i = map.find(key);
    if (i == map.end()) {
      node = new NTMNode<T>(key, now);
      map.insert(std::make_pair(key, node));
    } else {
      node = ((*i).second);
      int tdiff = now - node->timestamp;
      if ((tdiff < threshold_interval) && (node->count >= threshold_count)) {
        node->count++;
        return DENY;
      } else {
        // Reset the hit count if necessary
        if (tdiff >= threshold_interval) {
          node->count = 0;
          node->timestamp = now;
        }
      }
    }
    node->count++;
    return ALLOW;
  }

  /** Return the internal data about a key.
   *
   * @param key key
   */
  bool get_state(const T& key, int *count, time_t *stamp) {
    iterator i = map.find(key);
    if (i == map.end())
      return false;
    NTMNodePtr node = (*i).second;
    *count = node->count;
    *stamp = node->timestamp;
    return true;
  }

  /* Iterators, to access the internal state. */
  iterator begin() {
    return map.begin();
  }

  iterator end() {
    return map.end();
  }

private:
  /** Garbage collection.
   *
   * Removes entries older than a certain age. You shouldn't need to
   * call this function directly, cleanup happens automatically every
   * CLEANUP_TIME seconds.
   *
   * @param age gc age limit (seconds)
   */
  void cleanup(int age) {
    time_t now = system_timer->now();
    iterator i = map.begin(); 
    while (i != map.end()) {
      NTMNode<T> *node = (*i).second;
      if ((now - node->timestamp) > age) {
        delete node;
        map.erase(i++);
      } else {
        ++i;
      }
    }
    last_cleanup = now;
  }

  NTMMap map;
  int threshold_interval;       // in seconds
  int threshold_count;
  time_t last_cleanup;
};

} // namespace

#endif
