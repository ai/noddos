#!/bin/bash

### BEGIN INIT INFO
# Provides:          noddos-probe
# Required-Start:    $network $time
# Required-Stop:     $network $time
# Should-Start:      $syslog
# Should-Stop:       $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Starts the NoDDoS probe
### END INIT INFO

set -e

DAEMON=/usr/sbin/noddos-probe
NAME=noddos-probe
DESC="NoDDoS probe"
PIDDIR=/var/run
PIDFILE=${PIDDIR}/noddos-probe.pid
DEFAULTS_FILE=/etc/default/${NAME}
PROBE_OPTS=""
INTERFACE=eth0

WHITELIST_FILE=/etc/noddos/whitelist

test -x ${DAEMON} || exit 0

test -e ${DEFAULTS_FILE} && . ${DEFAULTS_FILE}

check_start() {
  if [ "${START}" != "1" ]; then
    log_action_msg "Not starting ${NAME}, START=0 in ${DEFAULTS_FILE}"
    exit 0
  fi
}

check_opts() {
  if [ -n "${INTERFACE}" ]; then
    PROBE_OPTS="${PROBE_OPTS} --interface=${INTERFACE}"
  fi
  if [ -n "${PORT}" ]; then
    PROBE_OPTS="${PROBE_OPTS} --port=${PORT}"
  fi
  if [ -z "${NODE_ID}" ]; then
    log_action_msg "Error: NODE_ID undefined in ${DEFAULTS_FILE}"
    exit 1
  fi
  PROBE_OPTS="${PROBE_OPTS} --node_id=${NODE_ID}"
  if [ -z "${SERVER}" ]; then
    log_action_msg "Error: SERVER undefined in ${DEFAULTS_FILE}"
    exit 1
  fi
  PROBE_OPTS="${PROBE_OPTS} --server=${SERVER}"
  if [ -e ${WHITELIST_FILE} ]; then
    PROBE_OPTS="${PROBE_OPTS} --whitelist=${WHITELIST_FILE}"
  fi
  if [ -n "${IPTABLES_CHAIN}" ]; then
    PROBE_OPTS="${PROBE_OPTS} --iptables_chain=${IPTABLES_CHAIN}"
  fi
  PROBE_OPTS="${PROBE_OPTS} --pidfile=${PIDFILE}"
}

. /lib/lsb/init-functions

case "$1" in
start)
  check_start
  check_opts
  log_daemon_msg "Starting ${DESC}" ${NAME}
  if start-stop-daemon --start --quiet --oknodo \
    --pidfile ${PIDFILE} --exec ${DAEMON} -- ${PROBE_OPTS}; then
    log_end_msg 0
  else
    log_end_msg 1
  fi
  ;;

stop)
  log_daemon_msg "Stopping ${DESC}" ${NAME}
  if start-stop-daemon --stop --quiet --oknodo \
    --pidfile ${PIDFILE}; then
    log_end_msg 0
  else
    log_end_msg 1
  fi
  ;;

restart)
  $0 stop
  sleep 1
  $0 start
  ;;

*)
  log_action_msg "Usage: $0 {start|stop|restart}"
  exit 1
esac

exit 0
