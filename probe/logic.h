// $Id$

#ifndef __logic_H
#define __logic_H 1

#include "common/acl.h"
#include "common/timer.h"
#include "common/ntm.h"
#include "common/status.h"
#include "net/dist.h"
#include "iptables_blacklist.h"
#include <iostream>
#include <cstring>
#include <math.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

namespace dos {

class QpsCounter
{
public:
  QpsCounter(int period_=120)
    : count(0), avg(0),
      period(period_),
      last_reset(system_timer->now())
  {}

  bool incr() {
    time_t now = system_timer->now();
    int elapsed = now - last_reset;
    count++;
    if (elapsed >= period) {
      last_reset = now;
      avg = count / (double)elapsed;
      count = 0;
      return true;
    } else {
      return false;
    }
  }

  double get_avg(){
    return avg;
  }

private:
  int count;
  int period;
  time_t last_reset;
  double avg;
};

// Some accessories to make in_addr work as a map key...

struct in_addr_eq {
  bool operator()(const in_addr a, const in_addr b) const {
    return (a.s_addr == b.s_addr);
  }
};

struct in_addr_hash {
  size_t operator()(in_addr x) const {
    return x.s_addr;
  }
};

class ProbeLogic
  : public dos::net::ProbeImpl
{
public:
  typedef in_addr key_type;
  typedef NTM<key_type, in_addr_hash, in_addr_eq> ntm_hash;

  ProbeLogic(int port_, int node_id_,
	     dos::net::MasterProxy *master_,
	     AclMatcher *whitelist_,
             const std::string& iptables_chain_)
    : node_id(node_id_),
      master(master_),
      whitelist(whitelist_),
      ntm(30, 100),
      alerted(300, 1),
      status(Status::OK),
      blacklist(iptables_chain_),
      dos::net::ProbeImpl(port_)
  {}

  void hit(u_int8_t proto, in_addr src, in_addr dst);

  virtual void handle_status(const std::string& status_);

  void set_status(Status status);

  Status get_status() const { return status; }

  int get_node_id() const { return node_id; }

  double get_qps() { return qps.get_avg(); }

  double get_qps_for(const std::string& ip, time_t now);

  std::string get_master_addr() { return master->master_addr; }

  std::vector<std::string> get_blacklist() { return blacklist.list(); }

  ntm_hash& get_ntm() { return ntm; }

private:
  int node_id;
  dos::net::MasterProxy *master;
  Status status;
  ProbeImpl probe_impl;
  QpsCounter qps;
  AclMatcher *whitelist;
  IptablesBlacklist blacklist;
  ntm_hash ntm;
  ntm_hash alerted;
};

} // namespace dos

#endif
