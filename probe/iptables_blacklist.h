// $Id$

#ifndef __probe_iptables_H
#define __probe_iptables_H 1

#include "common/blacklist.h"

namespace dos {

class IptablesBlacklist
  : public Blacklist
{
public:
  IptablesBlacklist(const std::string& chain_)
    : chain_name(chain_)
  {}

  virtual void enable(const std::string& ip);
  virtual void disable(const std::string& ip);

private:
  std::string chain_name;
};

} // namespace

#endif
