// $Id$

#include "logic.h"
#include "cap.h"
#include "common/log.h"
#include <iostream>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

extern "C" {
#include <pcap.h>
}

namespace dos {

/* How many bytes of each packet to capture */
#define SNAPLEN			68

#define ETHERNET_HDR_SIZE	14

struct ip_pkt {
  u_int8_t ip_vhl;		/* header length, version */
#define IP_V(ip)	(((ip)->ip_vhl & 0xf0) >> 4)
#define IP_HL(ip)	((ip)->ip_vhl & 0x0f)
  u_int8_t ip_tos;		/* type of service */
  u_int16_t ip_len;		/* total length */
  u_int16_t ip_id;		/* identification */
  u_int16_t ip_off;		/* fragment offset field */
#define	IP_DF 0x4000            /* dont fragment flag */
#define	IP_MF 0x2000            /* more fragments flag */
#define	IP_OFFMASK 0x1fff       /* mask for fragmenting bits */
  u_int8_t ip_ttl;		/* time to live */
  u_int8_t ip_p;		/* protocol */
  u_int16_t ip_sum;		/* checksum */
  struct in_addr ip_src,ip_dst;	/* source and dest address */
};

// static void capture_callback(u_char *objptr,
//                              const struct pcap_pkthdr *pkthdr,
//                              const u_char *packet)
// {
//   IPProbe *probe = (IPProbe *)objptr;
//   probe->handle_packet(pkthdr, packet);
// }

static pcap_t *init_pcap(const char *interface, const char *filter)
  throw(PcapError)
{
  char errbuf[PCAP_ERRBUF_SIZE];
  pcap_t *descr;
  struct bpf_program fp;

  /* Open the pcap interface */
  *errbuf = '\0';
  descr = pcap_open_live(interface, SNAPLEN, 0, 1, errbuf);
  if (!descr)
    throw(new PcapError(errbuf));

  if (*errbuf)
    LOG4CXX_WARN(log::logger, "pcap_open_live(): " << errbuf);

  if (filter && *filter) {
    /* Compile the BPF filter */
    if (pcap_compile(descr, &fp, (char *)filter, 0, 0) < 0)
      throw(new PcapError("pcap_compile() failed"));

    /* Set the compiled program as the capture filter */
    if (pcap_setfilter(descr, &fp) < 0)
      throw(new PcapError("pcap_setfilter() failed"));
  }

  LOG4CXX_INFO(log::logger, "pcap init on " << interface
	       << ", filter=" << filter);

  return descr;
}

IPProbe *IPProbe::create(const char *interface, const char *filter, 
                         ProbeLogic *logic) throw(PcapError) 
{
  pcap_t *pcap = init_pcap(interface, filter);
  return new IPProbe(pcap, logic);
}

IPProbe::IPProbe(pcap_t *pcap_, ProbeLogic *logic_)
  : pcap(pcap_),
    dos::net::Event(pcap_fileno(pcap_)),
    logic(logic_)
{}

void IPProbe::ready() {
  struct pcap_pkthdr hdr;
  const u_char *packet = pcap_next(pcap, &hdr);
  if (packet)
    handle_packet(&hdr, packet);
}

void IPProbe::handle_packet(const struct pcap_pkthdr *pkthdr,
                            const u_char *packet)
{
  u_int caplen = pkthdr->caplen;
  u_int length = pkthdr->len;
  struct ip_pkt *ip;

  ip = (struct ip_pkt *)(packet + ETHERNET_HDR_SIZE);
  if (length < sizeof(struct ip_pkt))
    return;

  /* Check that the packet is correct IPv4, not truncated */
  u_int len = ntohs(ip->ip_len);
  u_int hlen = IP_HL(ip);
  u_int version = IP_V(ip);
  if (version != 4)
    return;
  if (hlen < 5)
    return;
  if (length < len)
    return;

  /* Check to see if we have the first fragment */
  u_int off = ntohs(ip->ip_off);
  if ((off & 0x1fff) == 0)
    logic->hit(ip->ip_p, ip->ip_src, ip->ip_dst);
}

} // namespace dos
