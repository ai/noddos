// $Id$

#ifndef __probe_status_httpd_H
#define __probe_status_httpd_H 1

#include "logic.h"
#include "net/httpd.h"

namespace dos {

class StatusHttpd
  : public net::Httpd
{
public:
  StatusHttpd(net::Async *async_, int port_, ProbeLogic *logic_);

  void handle_status(evhttp_request *req);

  void handle_dump_ntm(evhttp_request *req);

private:
  ProbeLogic *logic;
};

} // namespace dos 

#endif
