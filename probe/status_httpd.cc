// $Id$

#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include "status_httpd.h"

using namespace std;

namespace dos {

struct qps_entry
{
  string ip;
  double qps;
  int age;
};

struct sort_by_qps_descending {
  bool operator()(const qps_entry& a, const qps_entry& b) {
    return (a.qps > b.qps);
  }
};

StatusHttpd::StatusHttpd(net::Async *async_, int port_, ProbeLogic *logic_)
  : net::Httpd(async_, port_), logic(logic_)
{
  add_handler("/", httpd_callback(this, &StatusHttpd::handle_status));
  add_handler("/status", httpd_callback(this, &StatusHttpd::handle_status));
  add_handler("/ntm", httpd_callback(this, &StatusHttpd::handle_dump_ntm));
}

void StatusHttpd::handle_status(evhttp_request *req)
{
  ostringstream resp;

  resp << "<?xml version=\"1.0\"?>\n"
       << "<html lang=\"en\">\n"
       << "<head><title>NoDDoS probe " << logic->get_node_id()
       << "</title>\n"
       << "<link rel=\"stylesheet\" type=\"text/css\" href=\"/style.css\"/>\n"
       << "</head><body>\n";

  resp << "<h1>Probe #" << logic->get_node_id() << "</h1>"
       << "<p>Master at <a href=\"http://"
       << logic->get_master_addr() << "/\">"
       << logic->get_master_addr() << "</a></p>";

  resp << "<p>Current QPS: <b>" << logic->get_qps() << "</b></p>"
       << "<p>Current status: <b>" << logic->get_status().str() 
       << "</b></p>";

  resp << "<h3>Blacklisted hosts</h3><table class=\"d\">"
       << "<thead><tr><th>IP</th></tr></thead><tbody>";
  vector<string> blacklist = logic->get_blacklist();
  vector<string>::const_iterator i;
  for (i = blacklist.begin(); i != blacklist.end(); i++)
    resp << "<tr><td>" << (*i) << "</td></tr>";
  resp << "</tbody></table>\n";

  resp << "<p><a href=\"/ntm\">NTM table dump</a></p>";

  resp << "</body></html>\n";

  send_reply(req, 200, "OK", resp.str().c_str(), resp.str().size());
}

void StatusHttpd::handle_dump_ntm(evhttp_request *req)
{
  ostringstream resp;

  resp << "<?xml version=\"1.0\"?>\n"
       << "<html lang=\"en\">\n"
       << "<head><title>NoDDoS probe " << logic->get_node_id()
       << "</title>\n"
       << "<link rel=\"stylesheet\" type=\"text/css\" href=\"/style.css\"/>\n"
       << "</head><body>\n";

  resp << "<h1>Probe #" << logic->get_node_id() << "</h1>"
       << "<p>Master at <a href=\"http://"
       << logic->get_master_addr() << "/\">"
       << logic->get_master_addr() << "</a></p>";

  time_t now = system_timer->now();
  char addrbuf[16];
  vector<qps_entry> ips;
  ProbeLogic::ntm_hash::iterator i;
  for (i = logic->get_ntm().begin(); i != logic->get_ntm().end(); i++) {
    qps_entry q;
    q.age = now - (*i).second->timestamp;
    if ((*i).second->count > 0)
      q.qps = q.age / (double)((*i).second->count);
    else
      q.qps = 0;
    q.ip.assign(inet_ntoa((in_addr)((*i).first)));
    q.ip.assign(addrbuf);
    ips.push_back(q);
  }
  sort(ips.begin(), ips.end(), sort_by_qps_descending());

  resp << "<table><thead>"
       << "<tr><th>IP</th><th>qps</th><th>freshness</th></tr>"
       << "</thead><tbody>";
  vector<qps_entry>::const_iterator q;
  for (q = ips.begin(); q != ips.end(); q++)
    resp << "<tr><td>" << q->ip << "</td><td>" << q->qps 
         << "</td><td>" << q->age << "</td></tr>\n";
  resp << "</tbody></table>\n";

  resp << "<p><a href=\"/\">Back</a></p>";

  resp << "</body></html>\n";

  send_reply(req, 200, "OK", resp.str().c_str(), resp.str().size());
}

} // namespace
