// $Id$

#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include "common/log.h"
#include "iptables_blacklist.h"

namespace dos {

static const char *IPTABLES = "/sbin/iptables";

static void exec_(const char * const cmd[]) {
  int pid, status = 0;

  pid = fork();
  if (pid == 0) {
    execv(IPTABLES, (char *const *)cmd);
    _exit(1);
  } else if (pid < 0) {
    LOG4CXX_ERROR(log::logger, "fork() failed: " << strerror(errno));
  } else {
    if (waitpid(pid, &status, 0) != pid) {
      LOG4CXX_ERROR(log::logger, "waitpid() failed");
      status = -1;
    }
    if (status != 0)
      LOG4CXX_WARN(log::logger, "iptables exited with status " << status);
  }
}

void IptablesBlacklist::enable(const std::string& ip) {
  const char *cmd[] = {IPTABLES,
                       "-D", chain_name.c_str(),
                       "--source", ip.c_str(),
                       "--jump", "DROP", 
                       NULL};
  exec_(cmd);
}

void IptablesBlacklist::disable(const std::string& ip) {
  const char *cmd[] = {IPTABLES,
                       "-I", chain_name.c_str(),
                       "--source", ip.c_str(),
                       "--jump", "DROP", 
                       NULL};
  exec_(cmd);
}

} // namespace dos

