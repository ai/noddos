// $Id$

#include "logic.h"
#include "cap.h"
#include "status_httpd.h"
#include "common/ntm.h"
#include "common/daemon.h"
#include "common/util.h"
#include "common/log.h"
#include <iostream>
#include <fstream>
#include <getopt.h>
#include <stdlib.h>

using namespace dos;
using namespace dos::net;

namespace {

// The default BPF filter expression: listen for udp, icmp and tcp SYN.
static char *default_bpf_filter = "icmp or (udp and not port 53) or (tcp[13] = 2)";

struct option longopts[] = {
  {"help", no_argument, NULL, 'h'},
  {"whitelist", required_argument, NULL, 'w'},
  {"interface", required_argument, NULL, 'i'},
  {"filter", required_argument, NULL, 'f'},
  {"pidfile", required_argument, NULL, 'P'},
  {"foreground", no_argument, NULL, 'F'},
  {"port", required_argument, NULL, 'p'},
  {"node_id", required_argument, NULL, 'n'},
  {"server", required_argument, NULL, 's'},
  {"iptables_chain", required_argument, NULL, 'C'},
  {NULL, 0, NULL, 0}
};

static void usage()
{
  std::cout << "Usage: probe [<OPTIONS>]\n"
            << "Known options:\n"
	    << "  -n, --node_id NUM\n"
	    << "          Set the local node ID\n"
            << "  -w, --whitelist FILE\n"
            << "          Reads the whitelist from FILE (in CIDR format)\n"
            << "  -i, --interface DEVICE\n"
            << "          Capture packets on the specified interface\n"
            << "  -f, --filter BPF_FILTER\n"
            << "          Specify the desired BPF filter, as a single argument.\n"
            << "          (default: " << default_bpf_filter << ")\n"
	    << "  --pidfile FILE\n"
	    << "          Write the process pid to this file\n"
	    << "  --port PORT\n"
	    << "          Bind to this UDP port (default 6453)\n"
	    << "  -F, --foreground\n"
	    << "          Do not fork into the background\n"
	    << "  -s, --server HOST:PORT\n"
	    << "          Talk to the server at this address\n"
            << "  --iptables_chain CHAIN\n"
            << "          Add iptables blocks to the specified chain (default: INPUT)"
            << "\n";
  exit(0);
}

struct acl_matcher_adder {
  AclMatcher *whitelist;
  acl_matcher_adder(AclMatcher *w) : whitelist(w) {}
  bool operator()(const std::vector<std::string>& line) {
    if (line.size() != 1)
      return false;
    whitelist->add(line[0].c_str());
    return true;
  }
};

static AclMatcher *create_whitelist(const char *whitelist_file)
{
  AclMatcher *whitelist = new AclMatcher;
  if (whitelist_file)
    util::read_file(whitelist_file, acl_matcher_adder(whitelist));
  return whitelist;
}

static void run(int port, int node_id, const char *interface, 
		const char *whitelist_file, const char *server_addr,
		int server_port, const char *iptables_chain,
                const char *bpf_filter)
{
  try {
    Async async;
    MasterProxy master(server_addr, server_port);
    AclMatcher *whitelist = create_whitelist(whitelist_file);
    whitelist->add("127.0.0.0/8");
    whitelist->add("224.0.0.0/4");
    ProbeLogic *logic = new ProbeLogic(port, node_id, &master, 
                                       whitelist, iptables_chain);
    IPProbe *probe = IPProbe::create(interface, bpf_filter, logic);
    StatusHttpd httpd(&async, port, logic);
    logic->add(&async);
    probe->add(&async);
    async.run();
  } catch(std::runtime_error& e) {
    LOG4CXX_ERROR(log::logger, e.what());
  }
}

} // namespace

int main(int argc, char **argv)
{
  char *interface = NULL;
  char *whitelist_file = NULL;
  char *pid_file = NULL;
  char *bpf_filter = default_bpf_filter;
  bool background = true;
  int port = 6453;
  int node_id = 0;
  char server_addr[32] = "localhost";
  int server_port = 6454;
  char *iptables_chain = "INPUT";
  int c;

  while ((c = getopt_long(argc, argv, "df:Fw:i:p:hn:", longopts, NULL)) > 0) {
    switch (c) {
    case 'f':
      bpf_filter = optarg;
      break;
    case 'w':
      whitelist_file = optarg;
      break;
    case 'i':
      interface = optarg;
      break;
    case 'F':
      background = false;
      break;
    case 'P':
      pid_file = optarg;
      break;
    case 'p':
      port = atoi(optarg);
      break;
    case 'n':
      node_id = atoi(optarg);
      break;
    case 's':
      util::parse_addr(optarg, server_addr, &server_port);
      break;
    case 'C':
      iptables_chain = optarg;
      break;
    default:
      usage();
    }
  }
  if (!interface) {
    std::cerr << "Error: must specify a capture device with --interface\n";
    usage();
  }
  if (!node_id) {
    std::cerr << "Error: must specify a node ID\n";
    usage();
  }

  if (background)
    util::daemonize(0, pid_file);

  log::init(background);

  run(port, node_id, interface, whitelist_file, server_addr, 
      server_port, iptables_chain, bpf_filter);

  // unreached
  return 0;
}


