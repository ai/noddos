// $Id: acl_unittest.cc 41 2009-09-28 18:41:59Z ale $

#include <gtest/gtest.h>
#include "probe/logic.h"
#include "common/test/test_timer.h"

using namespace std;
using namespace dos;

namespace {

static const char *TEST_DB = "__test.db";

static TestTimer *mock_timer = new TestTimer();

class ControlledTimerEnvironment 
  : public testing::Environment
{
public:
  virtual void SetUp() {
    system_timer = mock_timer;
  }
};

TEST(QpsCounterTest, IncrementSanityCheck) {
  mock_timer->set(0);
  QpsCounter q(1);

  EXPECT_FALSE(q.incr());

  // check that qps is still 0 for the first period
  EXPECT_EQ(0, q.get_avg());

  // two hits, one tick -> qps=2
  mock_timer->set(1);
  EXPECT_TRUE(q.incr());
  EXPECT_EQ(2, q.get_avg());

  // testing sustained qps, again two hits, one tick
  EXPECT_FALSE(q.incr());
  mock_timer->set(2);
  EXPECT_TRUE(q.incr());
  EXPECT_EQ(2, q.get_avg());
}

} // namespace

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  testing::AddGlobalTestEnvironment(new ControlledTimerEnvironment);
  return RUN_ALL_TESTS();
}
