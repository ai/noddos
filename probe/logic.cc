// $Id$

#include "logic.h"
#include "common/log.h"
#include <iostream>
#include <string.h>
#include <arpa/inet.h>

namespace dos {

/*
 * Thresholds for host-based blacklisting
 */
struct threshold {
  int interval;
  int count;
} ntm_thresholds[] = {
  { 120, 600 },                 // OK:     5 qps for 2 minutes
  { 30, 90 },                   // ATTACK: 3 qps for 30 seconds
};

static const char *proto_to_str(u_int8_t proto) {
  switch(proto) {
  case 1:
    return "icmp";
  case 6:
    return "tcp";
  case 17:
    return "udp";
  default:
    return "other";
  }
}

void ProbeLogic::handle_status(const std::string& status_) {
  Status new_status(status_);
  set_status(new_status);
}

void ProbeLogic::set_status(Status new_status) {
  if (new_status != status) {
    status = new_status;
    threshold t = ntm_thresholds[status.value()];
    ntm.set_threshold(t.interval, t.count);
    LOG4CXX_INFO(log::logger, "global status changed to '" 
                 << status.str() << "'");
  }
}

void ProbeLogic::hit(u_int8_t proto, in_addr src, in_addr dst) {
  // Increment the qps counters
  if (qps.incr()) {
    // If the period expires, send our qps value to the master
    master->send_qps(node_id, qps.get_avg());
  }

  // Check the whitelist
  if (whitelist->match(src))
    return;

  // Check the timestamp map
  int n = ntm.hit(src);
  if (n == 1)
    return;

  // If already alerted, return
  n = alerted.hit(src);
  if (n == 0)
    return;

  char srcip[16], dstip[16];
  strcpy(srcip, inet_ntoa(src));
  strcpy(dstip, inet_ntoa(dst));

  // Blacklist the IP
  blacklist.add(srcip);
  master->send_blacklist(node_id, src);

  // Print a message
  LOG4CXX_INFO(log::logger, "blacklist: " << proto_to_str(proto) 
               << ": "  << srcip << " -> " << dstip);
}

double ProbeLogic::get_qps_for(const std::string& ip, time_t now) {
  in_addr src;
  int count;
  time_t stamp;
  inet_aton(ip.c_str(), &src);
  if (ntm.get_state(src, &count, &stamp))
    return count / (double)(now - stamp);
  else
    return 0;
}

} // namespace dos
