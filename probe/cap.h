// $Id$

#ifndef __cap_H
#define __cap_H 1

#include <stdexcept>
#include <sys/types.h>
#include <pcap.h>
#include "net/async.h"

namespace dos {

class ProbeLogic;

struct PcapError
  : public std::runtime_error
{
  PcapError(const char *what) throw()
    : std::runtime_error(what)
  {}
};

class IPProbe
  : public dos::net::Event
{
public:
  IPProbe(pcap_t *pcap_, ProbeLogic *logic_);

  virtual void ready();

  void handle_packet(const struct pcap_pkthdr *pkthdr,
                     const u_char *packet);

  static IPProbe *create(const char *interface, const char *filter, 
                         ProbeLogic *logic) throw(PcapError);

protected:
  pcap_t *pcap;
  ProbeLogic *logic;
};


} // namespace

#endif
